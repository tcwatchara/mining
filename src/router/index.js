import { createRouter, createWebHistory } from 'vue-router';

import HomePage from '@/views/Home.vue'
import HomePageCh from '@/views/HomeCh.vue'
import AboutPage from '@/views/About.vue'
import AboutPageCh from '@/views/AboutCh.vue'
import OurProduct from '@/views/Our-Product.vue'
import OurProductCh from '@/views/Our-ProductCh.vue'
import OurMinePage from '@/views/Our-Mining-Process.vue'
import OurMinePageCh from '@/views/Our-Mining-ProcessCh.vue'
import SustainbilityPage from '@/views/Sustainbility.vue'
import SustainbilityPageCh from '@/views/SustainbilityCh.vue'
import ContactPage from '@/views/Contact.vue'
import ContactPageCh from '@/views/ContactCh.vue'

const path = '';
//const path = '';
const routes = [
  
    { path: path, redirect: { path: path+"/Home" }},
    { path: path+"/Home", name: "Home",component: HomePage},
    { path: path+"/Home/ch", name: "HomeCh",component: HomePageCh},
    { path: path+"/About-us", name: "About",component: AboutPage},
    { path: path+"/About-us/ch", name: "AboutCh",component: AboutPageCh},
    { path: path+"/Our-Product", name: "OurProduct",component: OurProduct},
    { path: path+"/Our-Product/ch", name: "OurProductCh",component: OurProductCh},
    { path: path+"/Our-Mining-Process", name: "OurMine",component: OurMinePage},
    { path: path+"/Our-Mining-Process/ch", name: "OurMineCh",component: OurMinePageCh},
    { path: path+"/Sustainbility", name: "Sustainbility",component: SustainbilityPage},
    { path: path+"/Sustainbility/ch", name: "SustainbilityCh",component: SustainbilityPageCh},
    { path: path+"/Contact-us", name: "Contact",component: ContactPage},
    { path: path+"/Contact-us/ch", name: "ContactCh",component: ContactPageCh},
]


let router = createRouter({
    history: createWebHistory(),
    routes,
})

export default router;